export interface countryModel {
    countryId: string;
    countryName: string;
}

export interface countryModelResponse extends countryModel {
      status: boolean;
      errorMsg: string;
      data: countryModel[];
}

export interface report {
        id: Number,
        main: String,
        description: String,
        icon: Number
}

export interface weatherModel {
    coord: {
        lon: Number,
        lat: Number
    },
    weather: report[],
    base: String,
    main: {
        temp: Number,
        pressure: Number,
        humidity: Number,
        temp_min: Number,
        temp_max: Number
    },
    visibility: Number,
    wind: {
        speed: Number,
        deg: Number
    },
    clouds: {
        all: Number
    },
    dt: Number,
    system: {
        type: Number,
        id: Number,
        message: Number,
        country: String,
        sunrise: Number,
        sunset: Number
    },
    id: Number,
    name: String,
    cod: Number
}

export interface weatherModelResponse extends weatherModel {
    status: boolean;
    errorMsg: string;
    data: weatherModel;
}
