import { Component, OnInit ,Input} from '@angular/core';
import { countryModel, weatherModel, } from '../../models/model';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  @Input() country: countryModel;
  @Input() report: weatherModel;

  public noWeatherData: string = 'weather data not available now for this country';
  
  constructor() { }

  ngOnInit(): void {
  
  }

}
