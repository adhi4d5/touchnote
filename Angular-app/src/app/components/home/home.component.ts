import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { countryModel, countryModelResponse, weatherModel, weatherModelResponse } from '../../models/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public selectedCountry: countryModel;
  public countries: countryModel [] = [];
  public weatherReport: weatherModel ;
  public showReport: boolean;
  public showloader: boolean;
  public errorMsg: string;
  public coutriesListEmpty: string = 'NO countires are available to select.';

  constructor(private ApiService: ApiService) { }

  ngOnInit(): void {
  this.getCountries();
  }

  getCountries(){
  this.ApiService.getCountriesList().subscribe((response: countryModelResponse)=>{
    console.log("response",response);
    if(response.status){
     this.countries = response.data;
    }
   },(error) => {
    console.log("error",error);
    this.errorMsg = error.errorMsg;
   })
  }

  getweatherReport(){
    this.showReport = false;
    this.showloader = true;
    this.ApiService.getWeatherReport(this.selectedCountry).subscribe((response: weatherModelResponse)=>{
      if(response.status){
      this.weatherReport = response.data;
      this.showReport = true;
      this.showloader = false;
      }
    },(error) => {
      this.showloader = false;
      this.errorMsg = error.errorMsg;
     })
    }

}
