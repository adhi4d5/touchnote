import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { countryModelResponse, weatherModelResponse } from '../models/model';
import { environment } from '../../environments/environment';
import { API_ENDPOINTS } from '../models/apiEndpoints';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getCountriesList(){
    return this.http.get<countryModelResponse>(`${environment.apiUrl}/${API_ENDPOINTS.API_COUNTRY}`);
  }

  getWeatherReport(country){
    return this.http.post<weatherModelResponse>(`${environment.apiUrl}/${API_ENDPOINTS.API_WEATHER}`,country);
  }
}
