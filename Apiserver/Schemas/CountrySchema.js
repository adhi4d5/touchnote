var mongoose = require('mongoose');
var CountryModel = new mongoose.Schema({
    countryName: { type: String, required: true },
    countryId: { type: Number, required: true, unique: true }
});
var Model = mongoose.model('countryModel', CountryModel);
module.exports = Model;