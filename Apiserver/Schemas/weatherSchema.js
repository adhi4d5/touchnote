var mongoose = require('mongoose');
var weatherModel = new mongoose.Schema({
    coord: {
        lon: Number,
        lat: Number
    },
    weather: [{
        id: Number,
        main: String,
        description: String,
        icon: String
    }],
    base: String,
    main: {
        temp: Number,
        pressure: Number,
        humidity: Number,
        temp_min: Number,
        temp_max: Number
    },
    visibility: Number,
    wind: {
        speed: Number,
        deg: Number
    },
    clouds: {
        all: Number
    },
    dt: Number,
    system: {
        id: Number,
        message: Number,
        country: String,
        sunrise: String,
        sunset: String
    },
    id: { type: Number, required: true, unique: true },
    name: { type: String },
    cod: Number
});
var Model = mongoose.model('weatherModel', weatherModel);
module.exports = Model;