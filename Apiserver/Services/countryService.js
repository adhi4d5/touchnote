var Q = require('q');

/* Confiluration files */
var countrySchema = require('../Schemas/CountrySchema');
var configuration = require('../ConfigurationFiles/serverConfigurations.json');
/* Service */
var service = {}
service.getCountries = getCountries;

function getCountries() {
    var deferred = Q.defer();
    countrySchema.find({}, function(err, countries) {
        if (err) {
            deferred.reject({ status: false, errorMsg: "no records", data: [] });
        } else {
            deferred.resolve({ status: true, errorMsg: "", data: countries });
        }
    });
    return deferred.promise;
}

module.exports = service;