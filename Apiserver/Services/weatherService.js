var Q = require('q');
/* Confiluration files */
var weatherSchema = require('../Schemas/weatherSchema');
var configuration = require('../ConfigurationFiles/serverConfigurations.json');

/* Service */
var service = {}
service.getweatherReport = getweatherReport;

function getweatherReport(countryId) {
    var deferred = Q.defer();
    weatherSchema.find({ id: countryId }, function(err, weatherReport) {
        if (err) {
            deferred.reject({ status: false, errorMsg: "no records", data: [] });
        } else {
            deferred.resolve({ status: true, errorMsg: "", data: weatherReport[0] });
        }
    });
    return deferred.promise;
}

module.exports = service;