var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var config = require('./ConfigurationFiles/serverConfigurations.json');
var database = require('./ConfigurationFiles/databaseConfiguraion.json');
mongoose.Promise = global.Promise;
const mongodb = mongoose.connect(database.mongourl);
mongodb.then((db) => {
    // console.log(db.connections)
    console.log('Connected to MongoDB on', db.host + ':' + db.port);
})
var app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

//User Panel Routers
app.use('/api/country', require('./Controllers/countryController'));
app.use('/api/weather', require('./Controllers/weatherController'));
require('./utility/loadedData');
///////////////////////// server configuration /////////////////////////////////////
if (config.secure) {
    https.createServer(options || {}, app).listen(config.port, function(req, res) {
        console.log('SSL Server Starts');
    });
} else {
    //config.port
    var server = app.listen(config.port, function() {
        console.log('Server listening at http://' + server.address().address + ':' + server.address().port);
    });
}