var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var countryService = require('../Services/countryService');

// routers for RestApi's
router.get('/Allcountries', Allcountries);

function Allcountries(req, res) {
    countryService.getCountries().then(function(response) {
        res.status(200).send(response);
    }).catch(function(err) {
        res.status(400).send(err);
    })
}
module.exports = router;