var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var weatherService = require('../Services/weatherService');

// routers for RestApi's
router.post('/getReportBYId', getweatherReport);

function getweatherReport(req, res) {
    weatherService.getweatherReport(req.body.countryId).then(function(response) {
        res.status(200).send(response);
    }).catch(function(err) {
        res.status(400).send(err);
    })
}
module.exports = router;