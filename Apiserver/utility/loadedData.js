var CountrySchema = require('../Schemas/CountrySchema');
var weatherSchema = require('../Schemas/weatherSchema');
let countryData = require('../utility/country.json');
let weatherData = require('../utility/weather.json');
defaulcountryList();
defaulweatherdata();
// console.log("countryData", countryData);
// console.log("weatherData", weatherData);

function defaulcountryList() {
    var options = { upsert: true, new: true, setDefaultsOnInsert: true };
    CountrySchema.insertMany(countryData, function(err, response) {
        if (err) {
            console.log("err in  defaulcountryList created");
        } else {
            console.log("default defaulcountryList loaded ");
        }
    })
}

function defaulweatherdata() {
    var options = { upsert: true, new: true, setDefaultsOnInsert: true };
    weatherSchema.insertMany(weatherData, function(err, response) {
        if (err) {
            console.log("err in  defaulweatherdata created");
        } else {
            console.log("default defaulweatherdata loaded ");
        }
    })
}